#include <SoftwareSerial.h>

// Pins Rx/Tx are 0 and 1
SoftwareSerial SoftSerial(0, 1);

// char array for the current card id
char card_id[13];  

// How many chars we've read
int count = 0;

// Current char being read
char data;

void setup() {
    // Baudrate is 9600
    SoftSerial.begin(9600);
    Serial.begin(9600);
}
 
void loop() {

    // Is a card being scanned
    if (SoftSerial.available()) {

        // Read from RFID
        // Only reads a single char
        data = SoftSerial.read();
        
        // Card ids start with a char 2 and end with a char 3
        // Start and stop chars are converted to parentheses
        if (data == 2 || data == 3) {
            data += 38;
        }
        
        // Add char to card_id and increment count
        card_id[count++] = data;

        // Card ids are 14 chars long
        // If count is 14, we've read 1 card id
        if (count == 14) {
            // Print to serial port
            Serial.print(card_id);

            // Reset count so we can read another card          
            count = 0;       
        }                  
    }
}
