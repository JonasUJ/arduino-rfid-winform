﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Management;

namespace Arduino_RFID
{
    class NoPortsConnectedException : Exception { }

    static class PortUtil
    {
        public static string[] Ports()
        {
            return SerialPort.GetPortNames();
        }

        public static SerialPort GetPort()
        {
            string[] ports = Ports();
            SerialPort port = new SerialPort();
            port.BaudRate = 9600;

            if (ports.Length == 0)
            {
                throw new NoPortsConnectedException();
            }

            port.PortName = ports[ports.Length-1];
            port.Open();
            return port;
        }
    }
}
