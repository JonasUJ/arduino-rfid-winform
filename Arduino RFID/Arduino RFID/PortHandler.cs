﻿using System;
using System.IO.Ports;

namespace Arduino_RFID
{
    class PortHandler : IDisposable
    {
        SerialPort port;
        public SerialPort Port { get => port; }

        private void Port_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            Console.WriteLine(e.EventType);
        }

        public PortHandler()
        {
            port = PortUtil.GetPort();
            port.PinChanged += Port_PinChanged;
        }

        public PortHandler(string portName)
        {
            port = new SerialPort(portName, 9600);
            port.PinChanged += Port_PinChanged;
        }

        public PortHandler(int baudRate)
        {
            port = PortUtil.GetPort();
            port.BaudRate = baudRate;
            port.PinChanged += Port_PinChanged;
        }

        public PortHandler(string portName, int baudRate)
        {
            port = new SerialPort(portName, baudRate);
            port.PinChanged += Port_PinChanged;
        }

        public SerialPort UpdatePort(string newName, int newBaudRate)
        {
            port.Close();
            //port = PortUtil.GetPort();
            port.PortName = newName;
            port.BaudRate = newBaudRate;
            port.Open();
            return port;
        }

        public void Dispose()
        {
            if (port != null && port.IsOpen)
            {
                port.Close();
            }
        }


    }
}
