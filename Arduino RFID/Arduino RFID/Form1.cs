﻿using System;
using System.Windows.Forms;

namespace Arduino_RFID
{
    public partial class Form1 : Form
    {
        PortHandler portHandler;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            portHandler = new PortHandler();
            Console.WriteLine(portHandler.Port.PortName);
            UpdatePortBox();
            
        }

        private void UpdatePortBox()
        {
            object selected = cb_selectedPort.SelectedItem;
            cb_selectedPort.Items.Clear();
            foreach (string p in PortUtil.Ports())
            {
                cb_selectedPort.Items.Add(p);
            }
            cb_selectedPort.SelectedItem = selected;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cb_selectedPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            portHandler.UpdatePort(cb_selectedPort.SelectedItem.ToString(), portHandler.Port.BaudRate);
        }
    }
}
